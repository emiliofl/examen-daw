// function sendRequest(){
//     let firstName = document.getElementById('exampleFirstName').value.trim();
//     let lastName = document.getElementById('exampleLastName').value.trim();
//     let mobilePhone = document.getElementById('mobilePhoneExample').value.trim();
//     let email = document.getElementById('exampleInputEmail').value.trim();
//     let exampleInputPassword = document.getElementById('exampleInputPassword').value.trim();
//     let exampleRepeatPassword = document.getElementById('exampleRepeatPassword').value.trim();

//     let registerForm = document.getElementById('registerForm');

//     if (firstName == "" || lastName == "" || mobilePhone == "" || email ==""
//         || exampleInputPassword == "" || exampleRepeatPassword == ""){
//             alert('Todos los campos son obligatorios!');
//             return;
//         }

//     if (exampleInputPassword == exampleRepeatPassword){
//         registerForm.submit();
//     }
//     else {
//         alert('Passwords doesn\'t match!');
//     }
    
// }

$(document).ready(function() {
    $('#registerForm').submit(function(event) {
        event.preventDefault();

        var formData = {
            name: $('#exampleFirstName').val().trim(),
            lastName: $('#exampleLastName').val().trim(),
            mobilePhone: $('#mobilePhoneExample').val().trim(),
            email: $('#exampleInputEmail').val().trim(),
            password: $('#exampleInputPassword').val().trim()
        };

        var repeatPassword = $('#exampleRepeatPassword').val().trim();

        if (formData.name == "" || formData.lastName == "" || formData.mobilePhone == "" 
            || formData.email =="" || formData.password == "" || 
            repeatPassword == ""){
            alert('Todos los campos son obligatorios!');
            return;
        }

        if (formData.password != repeatPassword)
        {
            alert('Passwords doesn\'t match!');
            return;
        }

        $('#registerButton1').hidden = true;
        $('#registerButton2').hidden = true;
        $('#registerButton3').hidden = true;

        $.ajax({
            type: 'POST',
            url: '/register',
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify(formData),
            statusCode: {
                404: function() {
                    alert('Servidor no encontrado.');
                },
                500: function() {
                    alert('Error interno del servidor.');
                },
            },
            success: function(response) {},
            error: function(response) {
                console.log(response.statusCode);
            }
        });

        $('#registerButton1').hidden = false;
        $('#registerButton2').hidden = false;
        $('#registerButton3').hidden = false;

        window.location.href = "/login";
    });
});