from flask import Blueprint, render_template, request
from flask_login import login_required, current_user
from .models import Comment
from . import db

main = Blueprint('main', __name__)

@main.route('/')
@login_required
def index():
    comments = Comment.query.filter_by(user_id=current_user.id).all()
    empty = not comments
    return render_template('index.html', title="SB Admin 2 - Home",comments=comments, empty=empty)

@main.route('/comments')
@login_required
def getComments():
    comments = Comment.query.filter_by(user_id=current_user.id)
    return comments

@main.route('/comment', methods=['POST'])
@login_required
def comment():
    content_type = request.headers.get('Content-Type')
    userId  = current_user.id

    if (content_type == 'application/json'):
        comment = request.json['commentText'].strip()
    if (not comment or comment == ""):
        flash('You have to write a comment before submitting!')
        return 'hello'

    new_comment = Comment(content=comment,user_id=userId)

    db.session.add(new_comment)
    db.session.commit()
    return 'bye'

@main.route('/forgotpwd')
def forgotpwd():
    return render_template('forgotpwd.html')